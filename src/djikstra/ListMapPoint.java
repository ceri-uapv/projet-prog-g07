package djikstra;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

import com.gluonhq.maps.MapPoint;


public class ListMapPoint {
	
	public List<MapPoint> listofMapPoint = new ArrayList<>();
	public Graph graph = new Graph();

	public ListMapPoint() {
	}
	
	public void initialise() throws IOException, ParserConfigurationException, SAXException, TransformerException {
		this.graph = graph.initialise();
		int i = 0;
        while(i != graph.nodes.size()-1) {
        	listofMapPoint.add(new MapPoint(graph.nodes.get(i).lat, graph.nodes.get(i).lon));
        	i++;
        }
	}
	
	public static void main (String[] args) throws IOException, ParserConfigurationException, SAXException, TransformerException {
		ListMapPoint l = new ListMapPoint();
		l.initialise();
		for(int i=0; i<l.listofMapPoint.size(); i++) {
			System.out.println(l.listofMapPoint.get(0).getLatitude());
			System.out.println(l.listofMapPoint.get(0).getLongitude());
		}
	}
}
