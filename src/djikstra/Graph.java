package djikstra;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map.Entry;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import java.util.Set;

public class Graph {
	//private Set<Node> nodes = new HashSet<>(); // l'ensemble Des noeuds de graph
	public ArrayList<Node> nodes = new ArrayList<>();
	
	public void addNode(Node nodeA) {
        nodes.add(nodeA);
    }

	private static Double toRad(Double value) {
		 return value * Math.PI / 180;
	}
	public static double calculerLadistance(Double lat1,Double lon1,Double lat2, Double lon2) {
		 final int R = 6371;
		 Double latDistance = toRad(lat2-lat1);
		 Double lonDistance = toRad(lon2-lon1);
		 Double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + 
		 Math.cos(toRad(lat1)) * Math.cos(toRad(lat2)) * 
		 Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
		 Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
		 Double distance = R * c;
		 return distance;
		
	}
	public static Node plusProcheVoisin(Node source, Node Destination) throws IOException {
		File file = new File("xml\\care.fxml");
        FileInputStream fileStream = new FileInputStream(file);
        InputStreamReader input = new InputStreamReader(fileStream);
        BufferedReader reader = new BufferedReader(input);
        
        String line;
        Graph graph = new Graph();
        Node destinationSaved;
        Double distanceFromsource = Graph.calculerLadistance(source.lat,source.lon,Destination.lat,Destination.lon);
		
        double distancePrecedent = Double.MAX_VALUE;
        double distanceCurrent = 0.0;
        double distanceCurrentFromDesti;
        String id = null;
        String maLat = null;
        String maLon = null;
		while ((line = reader.readLine()) != null) {
			if(line.contains("<node")) {
				String [] pass2 = line.split("id=\"");
				String idNode1 = pass2[1];
				String[] idNode = idNode1.split("\" lat=\"");
				String [] latitude = idNode[1].split("\" lon=\"");
				//System.out.print(latitude[0] +" ");//latitude		
				String [] longitude = latitude[1].split("\"");
				//System.out.println(longitude[0]); // longitude
				distanceCurrent = Graph.calculerLadistance(source.lat,source.lon,Double.parseDouble(latitude[0]),Double.parseDouble(longitude[0]));
				distanceCurrentFromDesti = Graph.calculerLadistance(Destination.lat,Destination.lon,Double.parseDouble(latitude[0]),Double.parseDouble(longitude[0])); 				
				if(distanceCurrent != 0.0 && distanceCurrent < distancePrecedent && distanceCurrentFromDesti < distanceFromsource) {
					distancePrecedent = distanceCurrent;
					id = idNode[0];
					maLat = latitude[0];
					maLon = longitude[0];
					
					
				}
				
			}
		}
		reader.close();
		Node plusProcheVoisin = new Node(Long.parseLong(id),Double.parseDouble(maLat),Double.parseDouble(maLon));
		return plusProcheVoisin;
		
	}
	public Graph initialise() throws IOException, ParserConfigurationException, SAXException, TransformerException {
		
		Node source = new Node(970621989L,43.9172662,4.8715036);
        Node Destination = new Node(4778163432L,43.9425887, 4.8045569);
		Node plusProche = source;
        
		while(plusProche.lat != Destination.lat && plusProche.lon != Destination.lon) {
			plusProche =  plusProcheVoisin(source,Destination);
        	LinkedList<Node> shortestPath = new LinkedList<>();
	        shortestPath.add(source);
	        plusProche.setShortestPath(shortestPath);
	        source = plusProche;
        }
		LinkedList<Node> shortestPath = new LinkedList<>();
        shortestPath.add(plusProche);
        Destination.setShortestPath(shortestPath);
		source = new Node(970621989L,43.9172662,4.8715036);
		Graph graph = new Graph();
		graph.addNode(Destination);
		while(Destination.shortestPath.get(0).id != source.id) {
			graph.addNode(Destination);
			Destination = Destination.shortestPath.get(0);
		}
		return graph;
	}
}
