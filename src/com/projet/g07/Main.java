package com.projet.g07;
	
import java.awt.Graphics;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.gluonhq.maps.MapLayer;
import com.gluonhq.maps.MapPoint;
import com.gluonhq.maps.MapView;

import djikstra.ListMapPoint;
import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import marqueur.MarqueurArrivee;
import marqueur.MarqueurDepart;
/**
 * Le but du main est de mettre les diff�rentes classes en commun et  de produire un logiciel en Java reproduisant une
		partie des fonctionnalit�s propos�es par des services de cartographie
		
 * Variables globales contenant : 
 * Les donn�es de la scene : pageEntiere, top, centre
 * Les donn�es de la librairie GluonHQ : mapView, mapPoint qui est la localisation par d�faut et taille_zoom_act qui est le zoom par defaut
 * Les donn�es qui contiennent soit le cadre, soit l'itin�raire : djikstra.ListMapPoint listdepoints
 * Les donn�es que l'utilisateur va entrer : localisationDepart et localisationDestination
 * 
 */	
public class Main extends Application implements EventHandler<ActionEvent> {

	/*Les donn�es de la scene*/
	private StackPane pageEntiere;
	private  StackPane top;
	private StackPane centre;
	
	/*Les donn�es de la librairie GluonHQ*/
	MapView mapView;
	MapPoint mapPoint = new MapPoint(44.1170457,4.8760523);
	int taille_zoom_act=10;
	
	/*Les donn�es qui contiennent soit le cadre, soit l'itin�raire*/
	static djikstra.ListMapPoint listdepoints = new djikstra.ListMapPoint();
	
	/*Les donn�es que l'utilisateur va entrer*/
	static String localisationDepart;
	static String numeroDepart;
	static String rueDepart;
	static String villeDepart;
	static String localisationDestination;
	static String numeroDestination;
	static String rueDestination;
	static String villeDestination;
	/*
	 * C'est la m�thode qui va lancer le logiciel
	 * */
	public static void main(String[] args) throws IOException, ParserConfigurationException, SAXException, TransformerException {
		launch(args);
	}

	/*
	 * C'est la m�thode qui 
	 * */
    @Override
    public void start(Stage stage) throws IOException, ParserConfigurationException, SAXException, TransformerException { 
    		
    	  /* Cr�ation de la mise en page :
    	   * pageEntiere contient tout les Stackpane que ce soit le header (top) ou son contenu (centre)
    	   * le header top contient � les champs de texte et les boutons pour rechercher une ville, faire un itin�raire et r�gler le zoom
    	   * */
    	  pageEntiere = new StackPane();
    	  
    	  // Debut du header (top)
    	  top = new StackPane();										//On instancie top
          BorderPane cadre = new BorderPane();							//On cr�e un cadre o� mettre les champs et les boutons
         
          HBox header = new HBox(6);									//
          header.setPadding(new Insets(10, 10, 10, 10));
          
          Button connexion = new Button("Se connecter");
          connexion.setPadding(new Insets(10, 10, 10, 10));
          Button inscription = new Button("S'inscrire");
          inscription.setPadding(new Insets(10, 10, 10, 10));
          
          
          //Elements allerA
          /*Tout ce qui concerne une localisation ou un itin�raire*/
          
          
          /*Creation d'un Hbox pour mettre tout en ligne*/
          HBox allerA = new HBox(5);
          
          /*Saisir la localisation souhait�e*/
          TextField area = new TextField();
          area.setPromptText("Saisir localisation");
          area.setMinWidth(120);
          area.setPadding(new Insets(10, 10, 10, 10));
          
          /*Bouton pour valider la requ�te*/
          Button goToLocation = new Button("Aller");
          goToLocation.setPadding(new Insets(10, 10, 10, 10));
          goToLocation.setOnAction(e -> { Main.localisationDepart = area.getText(); 
        	  //afficherLocalisation();
        	  try {
        		listdepoints.listofMapPoint.clear();
				searchCadre();
				listdepoints.initialiseCadre();
				MapPoint mapPoint = new MapPoint(listdepoints.listofMapPoint.get(0).getLatitude(), listdepoints.listofMapPoint.get(0).getLongitude());
				Poly p = new Poly(listdepoints.listofMapPoint);
		    	mapView.flyTo(0, mapPoint,3);
		    	mapView.setZoom(10);
		    	mapView.addLayer(p);
		    	mapPoint =	listdepoints.listofMapPoint.get(0);
		    	
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (ParserConfigurationException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (SAXException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (TransformerException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
          });
          
          /*Changer le mode de pour un itin�raire*/
          File fleches = new File("images/arrows.png");
          Image fle = new Image(fleches.toURI().toString());
          ImageView imageFleche = new ImageView(fle);
          imageFleche.setFitHeight(15);
          imageFleche.setFitWidth(15);
          Button changeTo = new Button();
          changeTo.setGraphic(imageFleche);
          changeTo.setPadding(new Insets(10, 10, 10, 10));
          
          changeTo.setOnAction(e -> { changeTop(); });
          
          /*Creation d'un Hbox pour mettre tout en ligne*/
          HBox itineraire = new HBox(5);
          
          /*Saisir la localisation souhait�e*/
          TextField depart = new TextField();
          depart.setPromptText("Saisir localisation de d�part");
          depart.setMinWidth(120);
          depart.setPadding(new Insets(10, 10, 10, 10));
          
          /*Bouton pour valider la requ�te*/
          TextField destination = new TextField();
          destination.setPromptText("Saisir localisation d'arriv�e");
          destination.setMinWidth(120);
          destination.setPadding(new Insets(10, 10, 10, 10));
          
          Button goToItineraire = new Button("Aller");
          goToItineraire.setPadding(new Insets(10, 10, 10, 10));
          goToItineraire.setOnAction(e -> { 
        	  try {
        		Main.localisationDepart = depart.getText(); 
            	Main.localisationDestination= destination.getText(); 
            	afficherItineraire(); 
				searchNodeForItineraire();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (ParserConfigurationException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (SAXException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (TransformerException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
          });
          
          /*Changer le mode de pour un itin�raire*/
          File close = new File("images/close.png");
          Image cross = new Image(close.toURI().toString());
          ImageView imageCross = new ImageView(cross);
          imageCross.setFitHeight(10);
          imageCross.setFitWidth(10);
          Button rechange = new Button();
          rechange.setGraphic(imageCross);
          rechange.setPadding(new Insets(10, 10, 10, 10));
          rechange.setOnAction(e -> { changeTop(); });
          
          /*Attribution des enfants aux Hbox*/
          allerA.getChildren().addAll(area,goToLocation, changeTo);
          itineraire.getChildren().addAll(depart,destination,goToItineraire, rechange);
          
          /*Attribution des enfants aux Hbox*/
          allerA.setVisible(true);
          itineraire.setVisible(false);
          // Button zoom
          File add = new File("images/add.png");
          Image add_c = new Image(add.toURI().toString());
          ImageView imageadd = new ImageView(add_c);
          imageadd.setFitHeight(20);
          imageadd.setFitWidth(20);
          Button zoom = new Button();
          zoom.setPadding(new Insets(10, 10, 10, 10));
          zoom.setGraphic(imageadd);
          zoom.setOnAction(this);
          // button pour dezoomer
          File subt = new File("images/subt.png");
          Image subt_c = new Image(subt.toURI().toString());
          ImageView imagesubt = new ImageView(subt_c);
          imagesubt.setFitHeight(20);
          imagesubt.setFitWidth(20);
          Button dezoom = new Button();
          dezoom.setPadding(new Insets(10, 10, 10, 10));
          dezoom.setGraphic(imagesubt);
          dezoom.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent arg0) {
				mapView.setZoom(--taille_zoom_act);
				
			}
		});
          //Fin header
          
          /*Creation d'un autre Stack*/
          centre = new StackPane();
    	  VBox carte = new VBox();
    	  /* Cr�ation de la carte Gluon JavaFX */
    	  mapView = new MapView();
    	  
    	  top.getChildren().addAll(itineraire,allerA);
          header.getChildren().addAll(top,connexion, inscription,zoom,dezoom);
          cadre.setTop(header);
    	  /* Cr�ation du point avec latitude et longitude */
          
//          MapPoint mapPoint = new MapPoint(44.1170457,4.8760523);
//          MapPoint mapPoint = new MapPoint(listdepoints.listofMapPoint.get(0).getLatitude(), listdepoints.listofMapPoint.get(0).getLongitude());
    	  
    	  mapView.setOnMouseClicked(e -> {
    		  mapView.flyTo(5, mapPoint,10);
    	  });
    	  
    	  /* Cr�ation et ajoute une couche � la carte */
          
          Itineraire it = new Itineraire();
 
//    	  MapLayer mapLayer = new MarqueurDepart(mapPoint);
//    	  mapView.addLayer(mapLayer);
    	 
    	  /* Zoom de 5 */
    	  mapView.setZoom(taille_zoom_act);

//    	  MapPoint arrivee = new MapPoint(listdepoints.listofMapPoint.get(listdepoints.listofMapPoint.size()-1).getLatitude(), listdepoints.listofMapPoint.get(listdepoints.listofMapPoint.size()-1).getLongitude());
//    	  MapLayer arriveeMarqueur = new MarqueurArrivee(arrivee);
//    	  mapView.addLayer(arriveeMarqueur);
    	  
    	  /* Centre la carte sur le point */
    	  mapView.flyTo(0, mapPoint, 0.1);
    	  carte.getChildren().add(mapView);
    	  centre.getChildren().addAll(carte);
    	  cadre.setCenter(centre);
    	  //Fin element centrale
    	  
    	  pageEntiere.getChildren().add(cadre);
    	  /*
    	   * IMPORTANT mettre la taille de la fen�tre pour �viter l'erreur
    	   * java.lang.OutOfMemoryError
    	   */
    	  Scene scene = new Scene(pageEntiere, 1100, 800);

    	  stage.setScene(scene);
    	  stage.show();
     }
    
    private void changeTop() {
        ObservableList<Node> childs = this.top.getChildren();

        if (childs.size() > 1) {
            Node goToFront = childs.get(childs.size()-1);
            // Cette node va aller au premier-plan
            Node newGoToFront = childs.get(childs.size()-2);
                  
            goToFront.setVisible(false);
            goToFront.toBack();
          
            newGoToFront.setVisible(true);
        }
    }
    
    private void afficherLocalisation() {
    	System.out.println(localisationDepart);
    }
    
    
    private void afficherItineraire() {
    	System.out.println(localisationDepart);
    	String splitDepart[] = localisationDepart.split(" ");
    	Main.numeroDepart = splitDepart[0];
    	Main.rueDepart = "";
    	for(int i=1; i<splitDepart.length-1; i++) {
    		if(i==splitDepart.length-2) {
    			Main.rueDepart += splitDepart[i];
    		}
    		else {
    			Main.rueDepart += splitDepart[i]+ " ";
    		}
    	}
    	Main.villeDepart = splitDepart[splitDepart.length-1];
    	System.out.println(localisationDestination);
    	String splitDestination[] = localisationDestination.split(" ");
    	Main.numeroDestination = splitDestination[0];
    	Main.rueDestination = "";
    	for(int i=1; i<splitDestination.length-1; i++) {
    		if(i==splitDestination.length-2) {
    			Main.rueDestination += splitDestination[i];
    		}
    		else {
    			Main.rueDestination += splitDestination[i]+ " ";
    		}
    	}
    	Main.villeDestination = splitDestination[splitDestination.length-1];
    }

	@Override
	public void handle(ActionEvent event) {
		mapView.setZoom(++taille_zoom_act);
		
	}
	
	public static Document getNodesViaOverpass() throws IOException, ParserConfigurationException, SAXException {
		String hostname = "http://www.overpass-api.de/api/interpreter";
		String queryString = "way\r\n"
							+ "   [highway]\r\n"
							+ "  [area!=yes]\r\n"
							+ " (around:500,  43.9421466 , 4.8047645 , 43.9106016, 4.8905776);\r\n"
							+ "(._;>;);\r\n"
							+ "out;";
		URL osm = new URL(hostname);
		HttpURLConnection connection = (HttpURLConnection) osm.openConnection();
		connection.setDoInput(true);
		connection.setDoOutput(true);
		connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
		DataOutputStream printout = new DataOutputStream(connection.getOutputStream());
		printout.writeBytes("data=" + URLEncoder.encode(queryString, "utf-8"));
		printout.flush();
		printout.close();
	
		DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = dbfac.newDocumentBuilder();
		return docBuilder.parse(connection.getInputStream());
	}
	
	public static Document getCadreOverpass() throws IOException, ParserConfigurationException, SAXException {
        String hostname = "http://www.overpass-api.de/api/interpreter";
        String queryString = "relation\r\n"
                + "  [name="+Main.localisationDepart+"]\r\n"
                + "  [type=boundary]\r\n"
                + "  [admin_level = 8];\r\n"
                + "(._;>;);\r\n"
                + "out;";
        URL osm = new URL(hostname);
        HttpURLConnection connection = (HttpURLConnection) osm.openConnection();
        connection.setDoInput(true);
        connection.setDoOutput(true);
        connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        DataOutputStream printout = new DataOutputStream(connection.getOutputStream());
        printout.writeBytes("data=" + URLEncoder.encode(queryString, "utf-8"));
        printout.flush();
        printout.close();

        DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = dbfac.newDocumentBuilder();
        return docBuilder.parse(connection.getInputStream());
    }
	
	public void searchCadre() throws IOException, ParserConfigurationException, SAXException, TransformerException {
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource source = new DOMSource(getCadreOverpass());
        StreamResult streamResult =  new StreamResult(new File("xml//carteCadre.fxml"));
        transformer.transform(source, streamResult);
	}
	
	public static Document getNodesViaOverpassNodeForItineraire() throws IOException, ParserConfigurationException, SAXException {
		String hostname = "http://www.overpass-api.de/api/interpreter";
        String queryString = "area[name= \""+Main.villeDepart+"\"][type = \"boundary\"][admin_level=\"8\"]->.searchArea;\r\n"
                             + "node [\"addr:housenumber\"= \"" +Main.numeroDepart+"\"][\"addr:street\" = \"" + Main.rueDepart+"\"](area.searchArea);\r\n"
                             + "out;\r\n"
                             + ">;\r\n"
                             + "out;";
		System.out.println(queryString);
		URL osm = new URL(hostname);
        HttpURLConnection connection = (HttpURLConnection) osm.openConnection();
        connection.setDoInput(true);
        connection.setDoOutput(true);
        connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        DataOutputStream printout = new DataOutputStream(connection.getOutputStream());
        printout.writeBytes("data=" + URLEncoder.encode(queryString, "utf-8"));
        printout.flush();
        printout.close();

        DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = dbfac.newDocumentBuilder();
        return docBuilder.parse(connection.getInputStream());
	}
	
	public void searchNodeForItineraire() throws IOException, ParserConfigurationException, SAXException, TransformerException {
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource source = new DOMSource(getNodesViaOverpassNodeForItineraire());
        StreamResult streamResult =  new StreamResult(new File("xml//NodeItineraire.fxml"));
        transformer.transform(source, streamResult);
        System.out.println("ok bon");
	}
}